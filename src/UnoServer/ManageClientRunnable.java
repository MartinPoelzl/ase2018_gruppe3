package UnoServer;

import java.net.Socket;

public class ManageClientRunnable implements Runnable {
	
	private Socket socket;
	private GameManager manager;
	

	public Socket getSocket()
	{
		return socket;
	}


	public GameManager getManager()
	{
		return manager;
	}


	public ManageClientRunnable(Socket socket, GameManager manager) {
		super();
		this.socket = socket;
		this.manager = manager;
	}


	@Override
	public void run() {
		
	}

}

