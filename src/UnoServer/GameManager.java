package UnoServer;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

public class GameManager
{
	private int playerNum = 4;
	private Random random = new Random();
	private List<ClientRunnable> clients = new ArrayList<>();

	public GameManager()
	{
		try (FileInputStream fin = new FileInputStream("server.properties"))
		{
			Properties config = new Properties();
			config.load(fin);
			setPlayerNum(Integer.parseInt(config.getProperty("players.number")));
		} catch (NumberFormatException e1)
		{
			setPlayerNum(4);
		} catch (FileNotFoundException e1)
		{
			setPlayerNum(4);
		} catch (IOException e1)
		{
			System.out.println("Konfiguration konnte nicht gelesen werden: " + e1.getMessage());
			System.exit(-1);
		}

	}

	/**
	 * @return the playerNum
	 */
	public int getPlayerNum()
	{
		return playerNum;
	}

	/**
	 * @param playerNum
	 *            the playerNum to set
	 */
	public void setPlayerNum(int playerNum)
	{
		this.playerNum = playerNum;
	}

	/**
	 * @return the random
	 */
	public Random getRandom()
	{
		return random;
	}

	/**
	 * @param random
	 *            the random to set
	 */
	public void setRandom(Random random)
	{
		this.random = random;
	}

	/**
	 * @return the clients
	 */
	public List<ClientRunnable> getClients()
	{
		return clients;
	}

	/**
	 * @param clients
	 *            the clients to set
	 */
	public void setClients(List<ClientRunnable> clients)
	{
		this.clients = clients;
	}

	public boolean registerClient(ClientRunnable clientRunnable)
	{
		// TODO Auto-generated method stub
		return false;
	}

	public Map<String, Integer> getScores()
	{
		// TODO Auto-generated method stub
		return null;
	}
}
