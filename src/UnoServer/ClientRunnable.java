package UnoServer;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import UnoGame.Player;

public class ClientRunnable implements Runnable 
{
	private Socket client;
	private GameManager gm;
	private String Nickname;
	

	ClientRunnable(Socket client, GameManager gm) 
	{
		super();
		this.client = client;
		this.gm = gm;
	}

	public void run() {
		try (ObjectInputStream ois = new ObjectInputStream(client.getInputStream());
				ObjectOutputStream oos = new ObjectOutputStream(client.getOutputStream());) {
			Player p = (Player) ois.readObject();
			setNickname(p.getName());
			
			System.out.println(p.getName());
			
			if (!gm.registerClient(this)) {
				synchronized (this) {
					wait();
				}
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * @return the nickname
	 */
	public String getNickname()
	{
		return Nickname;
	}

	/**
	 * @param nickname the nickname to set
	 */
	public void setNickname(String nickname)
	{
		Nickname = nickname;
	}

}


