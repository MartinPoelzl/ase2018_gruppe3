package UnoUi;

public class CardDisplay
{
	String[][] board;
	String border = "*";
	String leer = " ";

	int bl; // boardlength
	int bw; // boardwidth
	int cs; // columnsize
	int rs; // rowsize

	// formt die R�nder der Karten
	public CardDisplay(String color, String value, String type)
	{

		bl = 7;
		bw = 5;
		
		board = new String[bl][bw];
		for (int row = 0; row < board.length; row++)
		{
			for (int column = 0; column < board[0].length; column++)
			{
				if (row == 0 || row == bl - 1)
				{
					board[row][column] = border;
				}

				else if (column == 0 || column == bw - 1)
				{
					board[row][column] = border;
				}

				else
				{
					board[row][column] = leer;
				}
			}
		}

		if (color.equals("N"))
		{
			color = " ";
		}

		if (Integer.parseInt(value) > 9)
		{
			value = " ";
		} else if (Integer.parseInt(value) == 20)
		{
			value = " ";
		}

		if (type.equals("takeTwo"))
		{

			type = "+";
			value = "2";
			cs = bl / 2 - 2;
			rs = bw / 2;
			board[cs][rs] = color;

			cs = bl / 4 + 2;
			rs = bw / 2;
			board[cs][rs] = type;

			cs = bl / 2 + 2;
			rs = bw / 2;
			board[cs][rs] = value;
		} else if (type.equals("changeColorTakeFour"))
		{
			color = "+";
			type = " ";
			value = "4";
			cs = bl / 2 - 1;
			rs = bw / 2;
			board[cs][rs] = color;

			cs = bl / 2;
			rs = bw / 2;
			board[cs][rs] = type;

			cs = bl / 2 + 1;
			rs = bw / 2;
			board[cs][rs] = value;
		} else if (type.equals("reverse"))
		{
			color = color.toUpperCase().substring(0, 1);
			type = " ";
			value = "r";
			cs = bl / 2 - 2;
			rs = bw / 2;
			board[cs][rs] = color;

			cs = bl / 4 + 2;
			rs = bw / 2;
			board[cs][rs] = type;

			cs = bl / 2 + 2;
			rs = bw / 2;
			board[cs][rs] = value;
		} else if (type.equals("number"))
		{
			color = color.substring(0, 1).toUpperCase();
			type = " ";

			cs = bl / 2 - 2;
			rs = bw / 2;
			board[cs][rs] = color;

			cs = bl / 4 + 2;
			rs = bw / 2;
			board[cs][rs] = type;

			cs = bl / 2 + 2;
			rs = bw / 2;
			board[cs][rs] = value;
		} else if (type.equals("changeColor"))
		{
			color = " ";
			type = "c";
			value = " ";
			cs = bl / 2 - 2;
			rs = bw / 2;
			board[cs][rs] = color;

			cs = bl / 4 + 2;
			rs = bw / 2;
			board[cs][rs] = type;

			cs = bl / 2 + 2;
			rs = bw / 2;
			board[cs][rs] = value;
		} else if (type.equals("skip"))
		{
			type = " ";
			value = "s";

			cs = bl / 2 - 2;
			rs = bw / 2;
			board[cs][rs] = color;

			cs = bl / 4 + 2;
			rs = bw / 2;
			board[cs][rs] = type;

			cs = bl / 2 + 2;
			rs = bw / 2;
			board[cs][rs] = value;
		}
	}

	@Override
	public String toString()
	{
		for (int row = 0; row < board.length; row++) // loop through columns
		{
			for (int column = 0; column < board[row].length; column++) // loop through column in row
			{
				System.out.print(board[row][column] + " ");
			}
			System.out.println();
		}
		return leer;
	}
}