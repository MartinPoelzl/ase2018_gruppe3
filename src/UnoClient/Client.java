package UnoClient;

import java.io.BufferedReader;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import UnoGame.Game;
import UnoGame.Player;


public class Client
{
	public static void main(String[] args)
	{
		try (Socket client = new Socket("172.32.0.85", 1122);
				ObjectOutputStream oos = new ObjectOutputStream(client.getOutputStream());
				ObjectInputStream ois = new ObjectInputStream(client.getInputStream());

				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				)
		{
			System.out.println("Spielernamen eingeben:");
			String name = br.readLine();
			Player p = new Player(name);
			oos.writeObject(p);
			oos.flush();
			
			while(true)
			{
				Game g = new Game();
				
					g.addPlayer(p);
				
			}
		} catch (UnknownHostException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
