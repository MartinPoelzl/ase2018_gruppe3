package UnoGame;

import UnoUi.*;

public class Card
{

	private CardDisplay display;
	private String color;

	// value f�r Spielende zum Zusammenz�hlen
	// bei NumberCard ist das die Nummer
	// bei WildCard oder ActionCard 50 bzw 20
	private int value;

	// types: number, takeTwo, skip, reverse, changeColor, changeColorTakeFour
	private String type;

	public Card()
	{
		super();
	}

	public Card(String color, Integer value, String type)
	{
		this.color = color;
		this.value = value;
		this.type = type;
		setDisplay(new CardDisplay(color.substring(0, 1).toUpperCase(), value.toString(), type));
	}

	public void setColor(String color)
	{
		this.color = color;
	}

	public String getColor()
	{
		return color;
	}

	public int getValue()
	{
		return value;
	}

	public String getType()
	{
		return type;

	}

	@Override
	public String toString()
	{
		
		return display.toString();
		
		// String output = null;
		//
		// if(type.equals("number")){
		// output = color + " " + value;}
		// else if(value == 20) {
		// output = color + " " + type;
		// }
		// else {
		// output = type;
		// }
		//
		// return output;
		//
		
//		if (value > 9)
//		{
//			System.out.print(display.toString());
//			return color + " " + type + "\n";
//		} else
//		{
//			System.out.print(display.toString());
//			System.out.println();
//			return color + " " + value + " " + "\n";
//		}

	}

	// passt eine Karte auf die aktuelle Karte am Ablagestapel

	public boolean match(Card cardFromStack)
	{
		Card cardFromPlayer = this;

		if (cardFromPlayer.getColor().equals(cardFromStack.getColor())) // Farbe passt
			return true;
		else if (cardFromPlayer.getType().equals("number") && cardFromPlayer.getValue() == cardFromStack.getValue()) // Nummer
																														// passt
			return true;
		// wenn Karte takeTwo, reverse oder skip ist
		else if (!cardFromPlayer.getType().equals("number") && !cardFromPlayer.getType().equals("changeColor")
				&& !cardFromPlayer.getType().equals("changeColorTakeFour"))
		{
			if (cardFromPlayer.getType().equals(cardFromStack.getType()))
				return true;
		} else if (cardFromPlayer.getValue() == 50) // Karte ist ein Farbwechsler (mit oder ohne plus 4)
			return true;

		return false;
	}

	/**
	 * @return the display
	 */
	public CardDisplay getDisplay()
	{
		return display;
	}

	/**
	 * @param display
	 *            the display to set
	 */
	public void setDisplay(CardDisplay display)
	{
		this.display = display;
	}

}
