package UnoGame;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Scanner;

import UnoDatenbank.SqliteRequests;

public class Gameloop {

	public static void main(String[] args) {

		// Scanner in der main anlegen und jedem Player mitübergeben

		Scanner sc = new Scanner(System.in);

		Game g = new Game();

		SqliteRequests sql = new SqliteRequests();

		// erstellt Tabelle in der Datenbank, falls noch keine existiert
		sql.init();

		 Player p1 = new Player();
		 //p1.setName();
		 g.addPlayer(p1);

		 //Player p2 = new Player();
		 //p2.setName();

		// Player p3 = new Player();
		// p3.setName();

		// Player p4 = new Player();
		// p4.setName();

		 //g.addPlayer(p2);
		// g.addPlayer(p3);
		// g.addPlayer(p4);
		//
		if (!g.playerArrayfull(g.getPlayers())) {

			g.generateBots();
		}

		// alle Player werden zur Session hinzugefügt und in die Datenbank eingetragen
		sql.startNewSession(g.getPlayers());

		int countRound = 1;
		int countReverse = 0;

		do {

			CardDeck c = new CardDeck();
			ArrayList<Card> deck = new ArrayList<Card>(c.getCardDeck());
			Collections.shuffle(deck);
			g.setCardDeck(deck);
			if (countReverse % 2 != 0) {
				g.reversePlayerOrder();
			}
			countReverse = 0;
			g.dealCards(g.getPlayers());
			g.setCardStack(g.putFirstCardOnCardStack());

			g.setCurrentPlayer(g.whoStarts(g.getPlayers()));

			do {

				if (g.howManyCardsAreThere(108) == false) {
					System.out.println("zu viele oder zu wenig Karten!!!");
					return;
				}

				Card currentStackCard = g.getCardStack().get(g.getCardStack().size() - 1);

				// zählt mit, wie oft reverse gespielt wurde
				if (currentStackCard.getType().equals("reverse")) {
					countReverse++;
				}
				System.out.println("-----------------");
				System.out.print("Card on Stack: \n");
				System.out.println();
				System.out.print(currentStackCard + "\n");
				System.out.println("-----------------");
				System.out.println("Current Player: \n" + g.getCurrentPlayer().getName());
				System.out.println("Handcards: ");
				System.out.println();
				System.out.println(g.getCurrentPlayer().getCards().toString().replace("[", "").replaceAll("]", "")
						.replaceAll(",", ""));
				System.out.print(g.getCurrentPlayer().getName() + " plays: \n");
				System.out.println();

				boolean validCard = false;

				while (validCard == false) {
					String action = null;
					boolean saidUno = false;
					String[] inputs = null;
					Card selectedCard = null;

					if (g.getCurrentPlayer().doesAnyCardMatch(currentStackCard)) {
						
						//läuft bis entweder s oder t eingegeben wurde (unabhängig von uno oder db abfragen
						boolean validActionSelected = false;
						
						while(!validActionSelected) {
						inputs = g.getCurrentPlayer().selectAction(currentStackCard);
						for (int i = 0; i < inputs.length; i++) {
							if (inputs[i].equals("s") || inputs[i].equals("t")) {
								action = inputs[i];
								validActionSelected = true;
							} else if (inputs[i].equals("uno")) {
								System.out.println("---------");
								System.out.println("|  " + inputs[i].toUpperCase() + "  |");
								System.out.println("---------");
								System.out.println();
								saidUno = true;
							} else if (inputs[i].equals("history")) {
								System.out.println("Player history: ");
								// gibt Player History aus
								ArrayList<HashMap<String, String>> history = sql
										.getHistory(g.getCurrentPlayer().getName());
								for (HashMap<String, String> hashMap : history) {
									System.out.println(hashMap.entrySet());
								}
							} else if (inputs[i].equals("session")) {
								System.out.println("Stats for current session: ");
								// gibt den Status der aktuellen Session aus (Player Namen und Punkte pro
								// Player)
								ArrayList<HashMap<String, String>> currentSessionStatus = sql.getAllSessionPoints();
								for (HashMap<String, String> hashMap : currentSessionStatus) {
									System.out.println(hashMap.entrySet());
								}
							}
						}
					}
					} else {
						action = "t";
					}
					

					if (action.equals("t")) {
						g.newCardDeck(); // prüft, ob noch Karten im Kartenstapel sind. Falls nicht, wird ein neuer
											// Kartenstapel aus dem Ablagestapel generiert
						System.out.println("you can´t play take a card :( \n");
						System.out.println(g.getCurrentPlayer().getName() + " takes: \n");
						System.out.println(g.getCardDeck().get(0));

						g.getCurrentPlayer().takeCard(g.getCardDeck().remove(0));
						// wenn die abgehobene Karte passt, kann sie abgelegt werden
						if (g.getCurrentPlayer().doesAnyCardMatch(currentStackCard)) {
							System.out.print(g.getCurrentPlayer().getName() + " plays: \n");
							System.out.println();
							continue;
						}

						g.setCurrentPlayer(g.nextPlayer(0));
						validCard = true;

					} else if (action.equals("s")) {

						selectedCard = g.getCurrentPlayer().selectCard(currentStackCard);
						if (selectedCard != null && selectedCard.match(currentStackCard)) {
							g.getCurrentPlayer().removeCard(selectedCard);
							g.getCardStack().add(selectedCard);
							// System.out.print(g.getCurrentPlayer().getName() + " plays: \n");
							// System.out.println();
							System.out.println(selectedCard);

							// UNO check
							if (!saidUno && g.getCurrentPlayer().howManyCardsLeft() == 1) {
								System.out
										.println("Hihi, you forgot to say uno. You have to take two cards from Stack");
								for(int i = 0; i<2;i++) {
								g.newCardDeck();
								g.getCurrentPlayer().takeCard(g.getCardDeck().remove(0));}
							} else if (saidUno && g.getCurrentPlayer().howManyCardsLeft() > 1) {
								System.out.println("Why did you say uno? Please learn how to count to 1...");
							}
						} else {
							System.out.println("No valid card selected. ");
							continue;
						}

						if (!g.getCurrentPlayer().hasCard()) {
							break;
						}

						String colorSelection = g.actionOrWildCardsArePlayed(selectedCard);

						if (selectedCard.getValue() == 50 && colorSelection != null) {
							System.out.println("Selected color: " + colorSelection);
							g.getCardStack().get(g.getCardStack().size() - 1).setColor(colorSelection);
						}

						validCard = true;
					}

				}

			} while (g.getCurrentPlayer().hasCard());
			System.out.println("********************************");
			System.out.println("Round over. " + g.getCurrentPlayer().getName() + " wins round " + countRound);
			System.out.println("********************************");
			System.out.println();

			countRound++;
			// Punkte werden summiert und dem entsprechenden Player zugeordnet
			g.getCurrentPlayer().setPointsPerRound(g.addUpPointsPerRound());
			g.addUpTotalPointsForRoundWinner(g.getCurrentPlayer());

			// speichert Punkte in die Datenbank
			sql.updateSession(g.getPlayers());

			// Alle Karten werden weggenommen und neu gemischt
			g.emptyPlayerHands();
			g.emptyCardStack();

			for (int i = 0; i < g.getPlayers().length; i++) {
				System.out.println(
						g.getPlayers()[i].getName() + " has " + g.getPlayers()[i].getTotalPoints() + " points");
			}
			System.out.println("");

		} while (!g.playerHas500PointsOrMore());

		System.out.println("┏(--)┛┗(--﻿ )┓┗(--)┛┏(--)┓\n");
		System.out.println(g.whoWinsTheGame().getName() + " you are the winner");
		System.out.println("\n┏(--)┛┗(--﻿ )┓┗(--)┛┏(--)┓");

	}
}
