package UnoGame;
import java.util.ArrayList;
import java.util.Collections;

public class CardDeck {

	
	// NumberCards generieren
	
	public ArrayList<Card> generateNumberCardList(){
	ArrayList<Card> numberCardList = new ArrayList<Card>();
	for(int i = 0;i<10;i++)
	{
		int counter = 0;
		if (i == 0) {
			numberCardList.add(new Card("red", i, "number"));
			numberCardList.add(new Card("green", i, "number"));
			numberCardList.add(new Card("blue", i, "number"));
			numberCardList.add(new Card("yellow", i, "number"));
		} else {
			while (counter < 2) {
				numberCardList.add(new Card("red", i, "number"));
				numberCardList.add(new Card("green", i, "number"));
				numberCardList.add(new Card("blue", i, "number"));
				numberCardList.add(new Card("yellow", i, "number"));
				counter++;
			}
		}
	}
	return numberCardList;
	}

	// ActionCards generieren (pro Farbe je 2 mal TakeTwo, Skip und Reverse)
	//enorm umst�ndlich programmiert, aber kann man sp�ter noch �ndern


	public ArrayList<Card> generateActionCardList(){
		 ArrayList<Card> actionCardList = new ArrayList<Card>();
	for(int j = 0; j<2; j++)
	{
		actionCardList.add(new Card("red",20,"takeTwo"));
		actionCardList.add(new Card("red",20,"skip"));
		actionCardList.add(new Card("red",20,"reverse"));
		actionCardList.add(new Card("blue",20,"takeTwo"));
		actionCardList.add(new Card("blue",20,"skip"));
		actionCardList.add(new Card("blue",20,"reverse"));
		actionCardList.add(new Card("green",20,"takeTwo"));
		actionCardList.add(new Card("green",20,"skip"));
		actionCardList.add(new Card("green",20,"reverse"));
		actionCardList.add(new Card("yellow",20,"takeTwo"));
		actionCardList.add(new Card("yellow",20,"skip"));
		actionCardList.add(new Card("yellow",20,"reverse"));		
	}
	return actionCardList;
	}
	
	//WildCards generieren (je 4 mal Farbwechsel und Farbwechsel+4)
	

	public ArrayList<Card> generateWildCardList(){
		ArrayList<Card> wildCardList = new ArrayList<Card>();
	for(int k = 0; k<4; k++) {
		wildCardList.add(new Card("none",50,"changeColor"));
		wildCardList.add(new Card("none",50,"changeColorTakeFour"));
	}
	return wildCardList;
	}
	
	//Kartenstapel generieren und auch gleich mischen
	

	public ArrayList<Card> getCardDeck() {
		ArrayList<Card> cardDeckList = new ArrayList<Card>();
		
		cardDeckList.addAll(generateNumberCardList());
		cardDeckList.addAll(generateActionCardList());
		cardDeckList.addAll(generateWildCardList());
		
		Collections.shuffle(cardDeckList);
		
		return cardDeckList;
	}
	
	

	}
	

