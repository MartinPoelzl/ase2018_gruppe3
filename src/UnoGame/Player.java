package UnoGame;

import java.util.ArrayList;
import java.util.Scanner;

public class Player
{

	private String Nickname = null;
	private ArrayList<Card> myCards = new ArrayList<>();
	private int pointsPerRound;
	private int totalPoints;

	public void setMyCardsOnEmpty()
	{
		myCards.clear();
	}

	public void setName(String name)
	{
		this.Nickname = name;
	}

	public String setName()
	{

		Scanner sc = new Scanner(System.in);
		String temp;

		do {
		System.out.println("Please insert Name:");
		temp = sc.nextLine();		
		}while(playerInputAllWhitespace(temp) || temp.contains("Bot"));

		return this.Nickname = temp;
	}
	
	public int getPointsPerRound()
	{
		return pointsPerRound;
	}

	public void setPointsPerRound(int pointsPerRound)
	{
		this.pointsPerRound = pointsPerRound;
	}

	public int getTotalPoints()
	{
		return totalPoints;
	}

	public void setTotalPoints(int totalPoints)
	{
		this.totalPoints = totalPoints;
	}

	public Player()
	{
		super();
		// TODO Auto-generated constructor stub
	}

	public Player(String name)
	{
		super();
		this.Nickname = name;
	}

	public String getName()
	{
		return Nickname;
	}

	public void takeCard(Card card)
	{
		myCards.add(card);
	}

	public boolean hasCard()
	{
		if (myCards.size() > 0)
		{
			return true;
		}
		return false;
	}

	public void removeCard(Card card)
	{

		myCards.remove(card);
	}

	public ArrayList<Card> getCards()
	{

		return myCards;
	}
	
	public String showMyCards(ArrayList<Card> cards)
	{
		return myCards.toString();
		
	}

	public int howManyCardsLeft()
	{
		return myCards.size();
	}

	public boolean wasUnoSaid(String input)
	{
		
		// TODO uno sagen implementieren

		return false;
	}

	public Card selectCard(Card currentStackCard)
	{

		Card cardToBePlayed = null;
		
		Scanner sc = new Scanner(System.in);

		while (cardToBePlayed == null)
		{			
			
			System.out.println("Please select the card you'd like to play \n");
			System.out.println("e.g. r4 for red 4 or ys for yellow skip, c for change color {n");
			System.out.println("br for blue reverse, r+2 for red takeTwo or +4 for color change plus 4\n");
			
			String input = sc.nextLine().toLowerCase();
			
			String inputType = null;
			String inputColor = null;
			int inputValue = 100; //null als default k�nnte bugs verursachen
			
			if(input.length()>0 && input.length()<4) {
				//hier kommt die Selektion der Karte
				if(input.length()==1 && input.equals("c")) {
					inputType = "changeColor";
					inputColor = "none";
				}
				else if(input.length()==1) {
					continue;
				}
				else if (input.length()==2 || input.length()==3){
					String ss1 = input.substring(0, 1);
					String ss2 = input.substring(1, 2);
					
					
					switch (ss1)
					{
					case ("+"):
						if(isInteger(ss2) && (Integer.parseInt(ss2))==4)
							inputType = "changeColorTakeFour";
							inputColor = "none";
						break;
					case ("r"):
						inputColor = "red";
						break;
					case ("b"):
						inputColor = "blue";
						break;
					case ("g"):
						inputColor = "green";
						break;
					case ("y"):
						inputColor = "yellow";
						break;
					default:
						System.out.println("Invalid selection. Try again. ");
						continue;
					}
					
					if(isInteger(ss2) && Integer.parseInt(ss2)>=0 && Integer.parseInt(ss2)<=9 && !inputColor.equals("none")) {
						inputType = "number";
						inputValue = Integer.parseInt(ss2);
											
					}
					else{					
						switch(ss2) {
						case ("r"):
							inputType = "reverse";
							break;
						case ("s"):
							inputType = "skip";
							break;
						case ("+"):
							if(input.length()==3) {
								String ss3 = input.substring(2, 3);
								if(isInteger(ss3) && Integer.parseInt(ss3)==2) {
									inputType = "takeTwo";
								}
							}
							else {
								System.out.println("Invalid selection. Try again. ");
								break;
							}
							break;
						case ("4"):
							break;
						default:
							System.out.println("Invalid selection. Try again. ");
							continue;
						}
					}
					
					
				}
				
			}
			else {
				System.out.println("Invalid input");
				continue;
			}
		
		//f�r NumberCards
		if(inputType.equals("number")) {
			for (Card card : myCards)
			{
				if (inputColor.equals(card.getColor()) && inputValue == card.getValue())
				{
					cardToBePlayed = card;
				}
			}
			}
		
		//sonst ist es eine ActionCard und folgende Pr�fung erfolgt
		else if (inputType.equals("skip") || inputType.equals("reverse") || inputType.equals("takeTwo")){
			for (Card card : myCards)
			{
				if (inputColor.equals(card.getColor()) && inputType == card.getType())
				{
					cardToBePlayed = card;
				}
			}
		}
		
		
		//wenn ein Farbwechsler gespielt wurde
		else if(inputColor.equals("none")) {
			for(Card card : myCards)
			{
				if(inputType.equals(card.getType())) {
					cardToBePlayed = card;
				}
			}
		}
			
			/*
			
			//Kartentyp w�hlen (number, takeTwo, reverse, skip, changeColor, changeColorTakeFour)
			do {
			System.out.println("Please select the card type (n, s, r, +4, +2, c) to be played or type x to exit: ");
			inputType = sc.next();
			
			switch (inputType)
			{
			case ("n"):
				inputType = "number";
				break;
			case ("+2"):
				inputType = "takeTwo";
				break;
			case ("r"):
				inputType = "reverse";
				break;
			case ("s"):
				inputType = "skip";
				break;
			case ("c"):
				inputType = "changeColor";
				break;
			case ("+4"):
				inputType = "changeColorTakeFour";
				break;
			case ("x"):
				break;
			default:
				System.out.println("Invalid selection. Try again. ");
				break;
			}
			}while(inputType == null);
			
			//wenn kein Farbwechsler, dann Farbe der Karte w�hlen
			if(inputType.equals("number") || inputType.equals("takeTwo") || inputType.equals("reverse") || inputType.equals("skip")) {
								
			inputColor = selectColor();
			if(inputColor == null) {
				return null;
			}
						
			//wenn NumberCard, value der Karte w�hlen
			if(inputType.equals("number")) {
				boolean validInputValue = false;
				do {
					System.out.println("Please select the value of the card you'd like to play or type x to exit: ");
					String temp = sc.next();
					if(isInteger(temp)) {
						inputValue = Integer.parseInt(temp);
						if(inputValue>=0 && inputValue<10) {
							validInputValue = true;
						}
					}
					else if(temp.equals("x")) {
						return null;
					}
					else {
						System.out.println("Invalid selection. Try again. ");
					}

				}while(!validInputValue);
				
				for (Card card : myCards)
				{
					if (inputColor.equals(card.getColor()) && inputValue == card.getValue())
					{
						cardToBePlayed = card;
					}
				}
			}
			//sonst ist es eine ActionCard und folgende Pr�fung erfolgt
			else {
				for (Card card : myCards)
				{
					if (inputColor.equals(card.getColor()) && inputType == card.getType())
					{
						cardToBePlayed = card;
					}
				}
			}
			}
			
			//wenn ein Farbwechsler gespielt wurde
			else if(inputType.equals("changeColor") || inputType.equals("changeColorTakeFour")) {
				for(Card card : myCards)
				{
					if(inputType.equals(card.getType())) {
						cardToBePlayed = card;
					}
				}
			}

			
		*/	
		}

		return cardToBePlayed;
	}

	// Spieler entscheidet, ob er eine Karte spielen oder vom Stack abheben will, bzw kann hier uno sagen oder nach history oder session info fragen
	public String[] selectAction(Card currentStackCard)
	{

		Scanner sc = new Scanner(System.in);
		String[] inputs = null;
		String selection = null;		
		boolean validSelection = false;
		while (validSelection == false)
		{

			System.out.println("Type 's' if you want to play or 't' to take a card from the deck (mandatory input!)");
			System.out.println("If you want some stats type 'history' to see your player history or 'session' for current session stats (optional input)");
			
			//hier kann man uno sagen (oder nach history und session info fragen), Eingabe muss durch Leerzeichen getrennt sein		
			String str = sc.nextLine();
			
			//checkt, ob auch wirklich etwas eingegeben wurde (nicht nur Leerzeichen)
			if(playerInputAllWhitespace(str)) {
				continue;
			}
			inputs = str.split(" ");
			
			//selection aus den inputs extrahieren
			int selectionCounter = 0;
			for(int i = 0; i < inputs.length; i++) {
				if(inputs[i].equals("s") || inputs[i].equals("t")) {
					selection = inputs[i];
					selectionCounter++;
				}
				
			}
			
			//falls es mehrere Eingaben gibt, die passen k�nnten, loop von vorne
			if(selectionCounter > 1) {
				System.out.println("Selection is not clear. Try again. ");
				continue;
			}
			else if(selectionCounter == 0) {
				System.out.println("You didn't select s or t. Try again!");
				return inputs;
			}
			

			if (selection.equals("s"))
			{
				validSelection = true;
			} else if (selection.equals("t"))
			{
				validSelection = true;
			}			
			else
			{
				System.out.println("Invalid selection. Try again.");
			}

		}

		return inputs;
	}

	// Spieler entscheidet, welche Farbe beim Farbwechsler gespielt wird
	public String selectColor()
	{

		Scanner sc = new Scanner(System.in);
		String selection = null;
		boolean validSelection = false;
		while (validSelection == false)
		{
			System.out.println("Select color to be played (r, g, y, b): ");
			selection = sc.next();


			switch (selection)
			{
			case ("r"):
				selection = "red";
				validSelection = true;
				break;
			case ("g"):
				selection = "green";
				validSelection = true;
				break;
			case ("y"):
				selection = "yellow";
				validSelection = true;
				break;
			case ("b"):
				selection = "blue";
				validSelection = true;
				break;
			case ("x"):
				return null;
			default:
				System.out.println("Invalid selection. Try again. ");
				break;
			}
		}

		return selection;

	}

	public boolean isEmpty()
	{
		// TODO Auto-generated method stub
		return true;
	}
	
	//checkt, ob eine Eingabe �berhaupt ein int ist
	public boolean isInteger(String input) {
		try {
	        Integer.parseInt( input );
	        return true;
	    }
	    catch( Exception e ) {
	        return false;
	    }
	}
	
	public boolean doesAnyCardMatch(Card currentStackCard) {
		
		for(Card card: myCards) {
			if(card.match(currentStackCard)) {
				return true;
			}
		}
		
		return false;
	}
	
	//checkt, ob auch wirklich etwas eingegeben wurde (nicht nur Leerzeichen)
	public boolean playerInputAllWhitespace(String s) {
		
		char[] c = s.toCharArray();
		int whitespaceCounter = 0;
		for(int i = 0; i < c.length; i++) {
			if(c[i] == ' ') {
				whitespaceCounter++;
			}
		}
		if(whitespaceCounter == c.length) {
			System.out.println("You only typed spaces. Give me something real to work with...");
			return true;

		}
		
		return false;
	}

}
