package UnoDatenbank;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import UnoGame.Player;

public class SqliteRequests
{
	private SqliteClient client;
	

//	erstellt eine Neue Tabelle Player mit Player ID PrimaryKey und den Spalten Nickname/ SessionIDPointsPerRound/ PointsOverall
	private static final String CREATETABLE = "CREATE TABLE Player (Nickname varchar(100) NOT NULL, SessionID int NOT NULL, Points int, CONSTRAINT PK_Players PRIMARY KEY (Nickname, SessionID));";
//	Befehl zum fÃ¼llen der Spalten
	private static final String INSERT_INTO_PLAYER = "INSERT INTO Player (Nickname, SessionID, Points) VALUES ('%1s', %2d, %3d);";
//	Befehl zum auswÃ¤hlen der Werte in den Spalten von Tabelle Player
	private static final String SELECT_SESSIONID= "SELECT SessionID from Player ORDER BY SessionID DESC LIMIT 1;";
	private static final String SELECT_BYPLAYER = "SELECT Nickname, Points, SessionID FROM Player WHERE Nickname = '%1s';";
	private static final String SELECT_BYSESSION = "SELECT Nickname, Points, SessionID FROM Player WHERE SessionID = %1d;"; 
	private static final String UPDATE_SESSION = "UPDATE Player SET Points = %1d  WHERE Nickname = '%2s' AND SessionID= %3d;";

	

	public SqliteRequests() {
		super();
		try {
			this.client = new SqliteClient("unoHighscore.sqlite");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void init()
	{
		try
		{
			if (!client.tableExists("Player"))
			{
				client.executeStatement(CREATETABLE);
			}

			
		} catch (SQLException ex)
		{
			System.out.println("Jetzt is wos schief gangen:" + ex.getMessage());
		}
	}

	public void startNewSession(Player[] players)
	{
		try
		{
				
			int currentSession = getSessionID()+1;
			
			for(int i = 0; i < players.length; i++) {				
				client.executeStatement(String.format(INSERT_INTO_PLAYER, players[i].getName(), currentSession, 0));
				
			}
			
			
		}catch(

	SQLException ex)
	{
		System.out.println("Name already exist: " + ex.getMessage());
	}

	}



	public int getSessionID()
	{
		int counter = 0;
		String SessionNum = "";
		ArrayList<HashMap<String, String>> result = null;
		HashMap<String, String> SessionMap = null;

		try
		{
			result = client.executeQuery((String.format(SELECT_SESSIONID)));
			if (result.isEmpty())
			{
				return 0;
			} else
			{
				SessionMap = result.get(0);
				SessionNum = SessionMap.get("SessionID");
				counter = Integer.valueOf(SessionNum);
			}

		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return counter;
	}
	
	
	public ArrayList<HashMap<String, String>> getAllSessionPoints()
	{

		ArrayList<HashMap<String, String>> punkte = null;

		try
		{
			int counter = getSessionID();
			punkte = client.executeQuery((String.format(SELECT_BYSESSION, counter)));

		} catch (SQLException ex)
		{
			System.out.println("Jetzt is wos schief gangen:" + ex.getMessage());
		}

		return punkte;
	}


	public void updateSession(Player[] players)
	{

		try
		{
			

			for(int i = 0; i < players.length; i++) {
				
				//String tempPoints = Integer.toString(players[i].getTotalPoints());
			
			client.executeStatement(String.format(UPDATE_SESSION, players[i].getTotalPoints(), players[i].getName(), getSessionID()));
			}

		} catch (SQLException ex)
		{
			System.out.println("Jetzt is wos schief gangen:" + ex.getMessage());
		}

	}

	public ArrayList<HashMap<String, String>> getHistory(String nickname)
	{
		ArrayList<HashMap<String, String>> result = null;

		try
		{
			result = client.executeQuery(String.format(SELECT_BYPLAYER, nickname));

		} catch (SQLException ex)
		{
			System.out.println("Jetzt is wos schief gangen:" + ex.getMessage());
		}
		return result;
	}



	

}